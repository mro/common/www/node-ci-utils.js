#!/usr/bin/env node
// @ts-check

import fs from "node:fs";
import path from "node:path";
import process from "node:process";
import { glob } from "glob";
import parseAuthor from "parse-author";
import yaml from "js-yaml";
import { Command } from "commander";

const cmd = new Command();

cmd
.option("--cwd <value>", "search directory", ".")
.option("--only-deps", "skip current project", false)
.option("--json", "output in plain json", false)
.option("-o, --outfile <file>", "output file (default: stdout)")
.option("-a, --append", "append to output file", false);

cmd.on("--help", () => {
  console.log("");
  console.log("Example:");
  console.log("  gen-credits '@cern/.*'");
});

cmd.parse(process.argv);
const options = cmd.opts();

/**
 * @typedef {{ name?: string, email?: string, url?: string }} Person
 * @typedef {{
 *  homepage?: string,
 *  name: string,
 *  author?: Person,
 *  version?: string,
 *  description?: string,
 *  contributors?: Person[],
 *  license: string }} PackageInfo
 */

/** @type {NodeJS.WritableStream} */
let outfile = process.stdout;
if (options.outfile) {
  const fd = fs.openSync(options.outfile, (options.append ? "a" : "w"), 0o655);
  outfile = fs.createWriteStream(options.outfile, { fd });
}

const ret = (cmd.args ?? []).reduce((ret, arg) => {
  try {
    glob.sync(arg, { cwd: path.join(options.cwd, "node_modules") })
    .forEach((path) => ret.push(path));
  }
  catch (err) {
    console.warn(`failed to glob "${arg}": ${err}`);
  }
  return ret;
}, /** @type {string[]} */ ([]));

process.chdir(path.join(options.cwd, "node_modules"));

/** @type {any} */
let pkglock;
try {
  pkglock = JSON.parse(
    fs.readFileSync(path.join("..", "package-lock.json")).toString());
}
catch (err) { console.warn(`failed to read package-lock.json: ${err}`); }

const db = {};

/**
 * @param {string|null} pkg
 * @param {any} db
 * @returns {any}
 */
function loadPkg(pkg, db) {
  try {
    const pkgjson = JSON.parse(fs.readFileSync(
      path.join(pkg ?? "..", "package.json")).toString());
    /** @type {PackageInfo} */
    const pkginfo = [ "homepage", "name", "author", "version", "description",
                      "contributors", "license" ].reduce((ret, prop) => {
      if (prop in pkgjson) {
        /** @ts-ignore */
        ret[prop] = pkgjson[prop];
      }
      return ret;
    }, /** @type {PackageInfo} */ ({}));
    db[pkginfo.name] = pkginfo;

    if (typeof pkginfo.author === "string") {
      pkginfo.author = parseAuthor(pkginfo.author);
    }
    if (pkginfo.contributors) {
      pkginfo.contributors = pkginfo.contributors.map(
        (c) => ((typeof c === "string") ? parseAuthor(c) : c));
    }

    if (pkg) {
      const hash = pkglock?.packages?.[`node_modules/${pkg}`]?.resolved;
      const hashIndex = (hash ?? "").lastIndexOf("#");
      if (hashIndex >= 0) {
        db[pkginfo.name].hash = hash.slice(hashIndex + 1);
      }
    }
    return db[pkginfo.name];
  }
  catch (err) {
    console.warn(`failed to read file "${pkg}": ${err}`);
  }
  return null;
}

if (!options.onlyDeps) {
  loadPkg(null, db);
}

ret.forEach((p) => loadPkg(p, db));

if (options.json) {
  outfile.write(JSON.stringify(db, null, 2));
  outfile.write("\n");
}
else {
  outfile.write(yaml.dump(db));
}

outfile.end();
