#!/usr/bin/env node
// @ts-check
// see: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool for details

import path from "node:path";
import readline from "readline";
import { Command } from "commander";

const cmd = new Command();

cmd
.option("-s, --src-path <value>", "source path");

cmd.on("--help", () => {
  console.log("");
  console.log("Example:");
  console.log("  npm run --silent lint:ci:builder | mkcodeclimate");
});

cmd.parse(process.argv);
const opts = cmd.opts();

/**
 * @typedef {{
 *  description: string,
 *  check_name: string
 *  location: { path: string, lines: { begin: number }},
 *  severity: "info" | "minor" | "major" | "critical" | "blocker"
 *  fingerprint: string
 * }} CodeClimateItem
 */

class Streamer {
  /**
   * @param {any} opts
   */
  constructor(opts) {
    this.lineRegex = /^(?<file>[/0-9a-zA-Z+._-]{1,}):(?<line>[0-9]{1,}):((?<char>[0-9]{1,}):)?\s*(?<message>.*)$/;
    this.count = 0;
    this.opts = opts;
    this.eslintExtraInfoRegex = /\[(?<severity>[^/]*)\/(?<check_name>[^\]]*)\]/;
  }

  /**
   * @param {CodeClimateItem} item
   * @return {CodeClimateItem}
   */
  eslintExtraInfo(item) {
    const match = item.description.match(this.eslintExtraInfoRegex);
    if (match) {
      item.severity = (match.groups?.severity?.toLowerCase() !== "error") ?
        "major" : "critical";
      // eslint-disable-next-line camelcase
      item.check_name = match.groups?.check_name ?? item.check_name;
      item.fingerprint = `${item.location.path}:${item.location.lines.begin}:${item.check_name}`;
    }
    return item;
  }

  /**
   * @param  {string} line
   */
  checkLine(line) {
    const match = line.match(this.lineRegex);
    if (match) {
      const cwd = this.opts?.srcPath;
      const file = match.groups?.file ?? "";

      const description = match.groups?.message ?? "";
      const location = {
        path: cwd ? path.relative(cwd, file) : file,
        lines: { begin: Number(match.groups?.line ?? "") }
      };

      /** @type {CodeClimateItem} */
      let ret = {
        description,
        location,
        severity: "major",

        // eslint-disable-next-line camelcase
        check_name: "lint", // default check_name
        fingerprint: `${location.path}:${location.lines.begin}:lint`
      };

      ret = this.eslintExtraInfo(ret);

      this.write(this.count++ ? ",\n  " : "\n  ");
      this.write(JSON.stringify(ret));
    }
  }

  async run() {
    this.count = 0;
    this.write("[");
    const prom = new Promise((resolve) => {
      const rl = readline.createInterface({ input: process.stdin });
      rl.on("line", (line) => this.checkLine(line));
      rl.on("close", () => {
        rl.removeAllListeners();
        rl.close();
        this.write("\n]");
        resolve(null);
      });
    });
    return prom;
  }

  /**
   * @param  {string} text
   */
  write(text) {
    process.stdout.write(text);
  }
}

const streamer = new Streamer(opts);
streamer.run();
