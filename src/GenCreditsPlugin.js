// @ts-check

import fs from "node:fs";
import path from "node:path";
import child from "node:child_process";

/**
 * @typedef {import('webpack').Compiler} Compiler
 */

/** @param {string} path */
function exists(path) {
  try {
    fs.accessSync(path);
    return true;
  }
  catch {
    return false;
  }
}

/**
 * @brief GenCreditsPlugin for webpack
 * @details will generate the Components.yml file using gen-credits utility
 */
export class GenCreditsPlugin {
  /**
   *
   * @param {string[]} [backdeps]
   * @param {string[]} [frontdeps]
   * @param {{ outfile?: string }} [args]
   */
  constructor(backdeps, frontdeps, args) {
    this.frontdeps = frontdeps;
    this.backdeps = backdeps;
    this.outfile = args?.outfile;
  }

  /**
   * @param {Compiler} compiler
   */
  apply(compiler) {
    compiler.hooks.initialize.tap("GenCredits", () => {
      let deps = this.backdeps ?
        this.backdeps.map((d) => `'${d}'`).join(" ") : "";

      if (exists(path.join("..", "package.json"))) {
        /* webapp project, backdeps are in parent directory */
        const outfile = this.outfile ?? "../Components.yml";
        child.execSync(`npx gen-credits ${deps} --cwd .. -o '${outfile}'`);
        if (this.frontdeps) {
          deps = this.frontdeps.map((d) => `'${d}'`).join(" ");
          child.execSync(
            `npx gen-credits ${deps} --only-deps -o '${outfile}' -a`);
        }
      }
      else {
        /* regular project */
        const outfile = this.outfile ?? "Components.yml";
        child.execSync(`npx gen-credits ${deps} -o '${outfile}'`);
      }
    });
  }
}

export default GenCreditsPlugin;
