// @ts-check

import { expect } from "chai";
import { describe, it } from "mocha";
import { execSync } from "node:child_process";
import { readFileSync } from "node:fs";
import path from "node:path";
import { fileURLToPath } from "node:url";

const dirname = path.dirname(fileURLToPath(import.meta.url));

describe("mkcodeclimate", function() {
  const exe = path.join(dirname, "..", "src", "mkcodeclimate.js");

  it("can generate a report", async function() {
    const data = readFileSync(path.join(dirname, "data", "eslint.log"));
    const ret = execSync(exe, { input: data });

    const doc = JSON.parse(ret.toString());
    expect(doc).to.have.length(1);
    expect(doc[0]).to.have.property("severity", "major");
  });

  it("can make paths relative", function() {
    const data = readFileSync(path.join(dirname, "data", "eslint.log"));
    const ret = execSync(
      `${exe} -s //home/fargie_s/work//CERN/sources/ntof/dim.js//`,
      { input: data });

    const doc = JSON.parse(ret.toString());
    expect(doc?.[0]?.location?.path).to.equal("src/index.test-d.ts");
  });
});
