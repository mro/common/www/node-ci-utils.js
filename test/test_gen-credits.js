// @ts-check

import { expect } from "chai";
import { describe, it } from "mocha";
import { execSync } from "node:child_process";
import path from "node:path";
import { fileURLToPath } from "node:url";

const dirname = path.dirname(fileURLToPath(import.meta.url));

describe("gen-credits", function() {
  const exe = path.join(dirname, "..", "src", "gen-credits.js");

  it("can generate a credits file", async function() {
    const ret = execSync(exe + " js-yaml --json");

    const doc = JSON.parse(ret.toString());
    expect(doc).to.have.property("@cern/ci-utils");
    expect(doc).to.have.property("js-yaml");
  });
});
