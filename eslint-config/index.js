/* eslint-disable max-lines */
module.exports = {
  parserOptions: {
    sourceType: "module",
    ecmaVersion: 12,
    ecmaFeatures: {
      jsx: true
    }
  },
  plugins: [
    "@typescript-eslint",
    "@stylistic/eslint-plugin-js"
  ],
  env: {
    amd: true,
    browser: false,
    jquery: true,
    node: true
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  rules: {
    "@typescript-eslint/no-useless-constructor": "error",
    "@typescript-eslint/no-var-requires": "off",
    "@typescript-eslint/ban-ts-comment": "off",
    "@typescript-eslint/no-empty-function": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/no-unused-vars": [
      "warn",
      {
        varsIgnorePattern: "^_",
        argsIgnorePattern: "^_"
      }
    ],
    "no-await-in-loop": "off",
    "no-cond-assign": "error",
    "no-console": "off",
    "no-constant-condition": "error",
    "no-control-regex": "error",
    "no-debugger": "error",
    "no-dupe-args": "error",
    "no-dupe-keys": "error",
    "no-duplicate-case": "error",
    "no-empty-character-class": "error",
    "no-empty": "error",
    "no-ex-assign": "error",
    "no-extra-boolean-cast": "error",
    "no-func-assign": "error",
    "no-inner-declarations": [
      "error",
      "functions"
    ],
    "no-invalid-regexp": "error",
    "no-irregular-whitespace": "error",
    "no-negated-in-lhs": "error",
    "no-obj-calls": "error",
    "no-prototype-builtins": "off",
    "no-regex-spaces": "error",
    "no-sparse-arrays": "error",
    "no-template-curly-in-string": "off",
    "no-unexpected-multiline": "error",
    "no-unreachable": "error",
    "no-unsafe-finally": "off",
    "no-unsafe-negation": "off",
    "use-isnan": "error",
    "valid-jsdoc": "off",
    "valid-typeof": "error",
    "accessor-pairs": "error",
    "array-callback-return": "off",
    "block-scoped-var": "off",
    "class-methods-use-this": "off",
    complexity: [
      "error",
      8
    ],
    "consistent-return": "error",
    curly: "off",
    "default-case": "off",
    eqeqeq: "error",
    "guard-for-in": "error",
    "no-alert": "error",
    "no-caller": "error",
    "no-case-declarations": "error",
    "no-div-regex": "error",
    "no-else-return": "off",
    "no-empty-function": "off",
    "no-empty-pattern": "error",
    "no-eq-null": "error",
    "no-eval": "error",
    "no-extend-native": "error",
    "no-extra-bind": "error",
    "no-extra-label": "off",
    "no-fallthrough": "error",
    "no-global-assign": "off",
    "no-implicit-coercion": "off",
    "no-implied-eval": "error",
    "no-invalid-this": "off",
    "no-iterator": "error",
    "no-labels": [
      "error",
      {
        allowLoop: true,
        allowSwitch: true
      }
    ],
    "no-lone-blocks": "error",
    "no-loop-func": "error",
    "no-magic-number": "off",
    "no-multi-str": "off",
    "no-native-reassign": "error",
    "no-new-func": "error",
    "no-new-wrappers": "error",
    "no-new": "error",
    "no-octal-escape": "error",
    "no-octal": "error",
    "no-param-reassign": "off",
    "no-proto": "error",
    "no-redeclare": "error",
    "no-restricted-properties": "off",
    "no-return-assign": "error",
    "no-return-await": "off",
    "no-script-url": "error",
    "no-self-assign": "off",
    "no-self-compare": "error",
    "no-sequences": "off",
    "no-throw-literal": "off",
    "no-unmodified-loop-condition": "off",
    "no-unused-expressions": "error",
    "no-unused-labels": "off",
    "no-useless-call": "error",
    "no-useless-concat": "error",
    "no-useless-escape": "off",
    "no-useless-return": "off",
    "no-void": "error",
    "no-warning-comments": "off",
    "no-with": "error",
    "prefer-promise-reject-errors": "off",
    radix: "error",
    "require-await": "off",
    "vars-on-top": "off",
    yoda: "off",
    strict: "off",
    "init-declarations": "off",
    "no-catch-shadow": "error",
    "no-delete-var": "error",
    "no-label-var": "error",
    "no-restricted-globals": "off",
    "no-shadow-restricted-names": "error",
    "no-shadow": "off",
    "no-undef-init": "error",
    "no-undef": "off",
    "no-undefined": "off",
    "no-unused-vars": "off",
    "no-use-before-define": "off",
    "callback-return": "error",
    "global-require": "error",
    "handle-callback-err": "error",
    "no-mixed-requires": "off",
    "no-new-require": "off",
    "no-path-concat": "error",
    "no-process-env": "off",
    "no-process-exit": "error",
    "no-restricted-modules": "off",
    "no-sync": "off",
    "@stylistic/js/array-bracket-newline": [
      "error",
      "consistent"
    ],
    "@stylistic/js/array-bracket-spacing": [
      "error",
      "always"
    ],
    "@stylistic/js/array-element-newline": "off",
    "@stylistic/js/arrow-parens": "error",
    "@stylistic/js/arrow-spacing": "error",
    "@stylistic/js/block-spacing": "error",
    "@stylistic/js/brace-style": [
      "error",
      "stroustrup",
      {
        allowSingleLine: true
      }
    ],
    "@stylistic/js/comma-dangle": "error",
    "@stylistic/js/comma-spacing": "error",
    "@stylistic/js/comma-style": "error",
    "@stylistic/js/computed-property-spacing": "error",
    "@stylistic/js/dot-location": [
      "error",
      "property"
    ],
    "@stylistic/js/eol-last": "warn",
    "@stylistic/js/function-call-argument-newline": "off",
    "@stylistic/js/function-call-spacing": "error",
    "@stylistic/js/function-paren-newline": "off",
    "@stylistic/js/generator-star-spacing": [
      "error",
      "after"
    ],
    "@stylistic/js/implicit-arrow-linebreak": [
      "error",
      "beside"
    ],
    "@stylistic/js/indent": [
      "error",
      2,
      {
        MemberExpression: 0,
        FunctionDeclaration: {
          parameters: "first"
        },
        ArrayExpression: "first",
        CallExpression: {
          arguments: 1
        },
        FunctionExpression: {
          body: 1,
          parameters: "first"
        }
      }
    ],
    "@stylistic/js/jsx-quotes": "warn",
    "@stylistic/js/key-spacing": "error",
    "@stylistic/js/keyword-spacing": "error",
    "@stylistic/js/linebreak-style": [
      "error",
      "unix"
    ],
    "@stylistic/js/lines-around-comment": "off",
    "@stylistic/js/lines-between-class-members": "error",
    "@stylistic/js/max-len": [
      "error",
      {
        code: 80,
        ignoreComments: true,
        ignorePattern: "/\\*\\* @type", // JSDoc types makes extra-long lines
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreRegExpLiterals: true
      }
    ],
    "@stylistic/js/max-statements-per-line": [
      "error",
      {
        max: 3
      }
    ],
    "@stylistic/js/multiline-ternary": "off",
    "@stylistic/js/new-parens": "error",
    "@stylistic/js/newline-per-chained-call": "off",
    "@stylistic/js/no-confusing-arrow": [
      "warn",
      {
        allowParens: true
      }
    ],
    "@stylistic/js/no-extra-parens": "off",
    "@stylistic/js/no-extra-semi": "error",
    "@stylistic/js/no-floating-decimal": "off",
    "@stylistic/js/no-mixed-operators": "warn",
    "@stylistic/js/no-mixed-spaces-and-tabs": "error",
    "@stylistic/js/no-multi-spaces": "error",
    "@stylistic/js/no-multiple-empty-lines": "warn",
    "@stylistic/js/no-tabs": "error",
    "@stylistic/js/no-trailing-spaces": "warn",
    "@stylistic/js/no-whitespace-before-property": "error",
    "@stylistic/js/non-block-statement-body-position": "off",
    "@stylistic/js/object-curly-newline": "off",
    "@stylistic/js/object-curly-spacing": [
      "error",
      "always"
    ],
    "@stylistic/js/object-property-newline": "off",
    "@stylistic/js/one-var-declaration-per-line": "off",
    "@stylistic/js/operator-linebreak": [
      "error",
      "after"
    ],
    "@stylistic/js/padded-blocks": "off",
    "@stylistic/js/padding-line-between-statements": "off",
    "@stylistic/js/quote-props": [
      "warn",
      "as-needed"
    ],
    "@stylistic/js/quotes": [
      "warn",
      "double",
      {
        avoidEscape: true,
        allowTemplateLiterals: false
      }
    ],
    "@stylistic/js/rest-spread-spacing": "error",
    "@stylistic/js/semi": "error",
    "@stylistic/js/semi-spacing": "error",
    "@stylistic/js/semi-style": "error",
    "@stylistic/js/space-before-blocks": "error",
    "@stylistic/js/space-before-function-paren": [
      "error",
      {
        anonymous: "never",
        named: "never",
        asyncArrow: "always"
      }
    ],
    "@stylistic/js/space-in-parens": "error",
    "@stylistic/js/space-infix-ops": "error",
    "@stylistic/js/space-unary-ops": [
      "error",
      {
        words: true,
        nonwords: false
      }
    ],
    "@stylistic/js/spaced-comment": [
      "error",
      "always",
      {
        exceptions: [
          "@flow"
        ],
        markers: [
          ":",
          "::",
          "*"
        ]
      }
    ],
    "@stylistic/js/switch-colon-spacing": "error",
    "@stylistic/js/template-curly-spacing": "warn",
    "@stylistic/js/template-tag-spacing": "off",
    "@stylistic/js/wrap-iife": "error",
    "@stylistic/js/wrap-regex": "off",
    "@stylistic/js/yield-star-spacing": "error",
    "dot-notation": "off",
    camelcase: [
      "error",
      {
        properties: "always"
      }
    ],
    "capitalized-comments": "off",
    "consistent-this": "off",
    "func-call-spacing": "error",
    "func-name-matching": "off",
    "func-names": "off",
    "func-style": "off",
    "id-length": "off",
    "id-match": "off",
    "implicit-arrow-linebreak": "error",
    "jsx-quotes": "off",
    "line-comment-position": "off",
    "lines-around-directive": "off",
    "max-depth": [
      "error",
      4
    ],
    "max-lines": [
      "error",
      {
        max: 300,
        skipComments: true
      }
    ],
    "max-nested-callbacks": "error",
    "max-params": [
      "error",
      6
    ],
    "max-statements": [
      "error",
      {
        max: 30
      }
    ],
    "new-cap": [
      "error",
      {
        capIsNew: false
      }
    ],
    "no-array-constructor": "error",
    "no-bitwise": "off",
    "no-continue": "off",
    "no-inline-comments": "off",
    "no-lonely-if": "warn",
    "no-multi-assign": "off",
    "no-negated-condition": "off",
    "no-nested-ternary": "error",
    "no-new-object": "error",
    "no-plusplus": "off",
    "no-restricted-syntax": "off",
    "no-spaced-func": "off",
    "no-ternary": "off",
    "no-underscore-dangle": "off",
    "no-unneeded-ternary": "off",
    "one-var": "off",
    "operator-assignment": "off",
    "require-jsdoc": "off",
    "sort-keys": "off",
    "sort-vars": "off",
    "unicode-bom": "error",
    "arrow-body-style": "off",
    "constructor-super": "error",
    "generator-star-spacing": "warn",
    "no-class-assign": "error",
    "no-const-assign": "error",
    "no-dupe-class-members": "error",
    "no-duplicate-imports": "error",
    "no-new-symbol": "error",
    "no-restricted-imports": "off",
    "no-this-before-super": "error",
    "no-useless-computed-key": "error",
    "no-useless-constructor": "off",
    "no-useless-rename": "warn",
    "no-var": "off",
    "object-shorthand": "off",
    "prefer-arrow-callback": "off",
    "prefer-const": "warn",
    "prefer-destructuring": "off",
    "prefer-numeric-literals": "warn",
    "prefer-rest-params": "off",
    "prefer-reflect": "off",
    "prefer-spread": "warn",
    "prefer-template": "off",
    "require-yield": "warn",
    "sort-imports": [
      "warn",
      {
        ignoreDeclarationSort: true
      }
    ],
    "symbol-description": "error"
  },
  overrides: [
    {
      files: [
        "*.ts",
        "*.mts",
        "*.cts",
        "*.tsx"
      ],
      rules: {
        "@typescript-eslint/explicit-module-boundary-types": "error",
        "@typescript-eslint/explicit-function-return-type": "error"
      }
    },
    {
      files: [ "*.d.ts" ],
      rules: {
        /* parent interfaces/namespaces may be exported */
        "@typescript-eslint/no-unused-vars": "off"
      }
    }
  ]
};
