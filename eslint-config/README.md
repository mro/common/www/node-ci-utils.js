# ESlint configuration files

This repository aggregates a set of configuration files for BE/CEM/MRO TeamA
developments.

## Installation

To install this package:
```bash
echo '@cern:registry=https://gitlab.cern.ch/api/v4/groups/mro/-/packages/npm/' > .npmrc

npm i --save-dev @cern/eslint-config
```

In your `.eslintrc.yml` :
```yml
---
root: true

env:
  amd: true
  browser: false
  jquery: true
  node: true

extends:
  - "@cern"
```
