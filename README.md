# Node.js CI tools and utilities

## Usage

In your Node.js (or front-end) project:
```bash
echo '@cern:registry=https://gitlab.cern.ch/api/v4/groups/mro/-/packages/npm/' > .npmrc

npm install --save-dev @cern/ci-utils
```

This will add various tools to your project, along with [eslint](https://eslint.org/) and [jshint](https://jshint.com/about/) as dependencies.

Those tools can either be used with [npx](https://docs.npmjs.com/cli/v7/commands/npx) or by adding `./node_modules/.bin` in your PATH.

## Integration

It is **recommended** to add the following scripts in your project's *package.json* file:
```js
{
  //...
  "scripts": {
    "test": "mocha test",
    "test:ci": "nyc -n src -a -r lcov -r text -r cobertura -- mocha test --reporter xunit --reporter-option output=tests.xml",
    "lint": "lint",
    "lint:ci:builder": "lint --builder",
    "lint:ci:report": "mkci-lint-report",
    "tc:ci:builder": "tc --builder",
    "tc:ci:report": "mkci-tc-report"
  }
}
```

## Tools: linting

The following linting utilities are available:
```bash
# Runs eslint and jshint
lint
# Using npx:
npx --no-install lint

# Runs eslint and jshint with easy to parse output
lint --builder

# Generate linting reports
mkci-lint-report
# Generates: lint_badge.svg, lint_report.html and lint_codeclimate.json
```

## Tools: typechecking

The following typechecking utilities are available:
```bash
# Runs tsc and tsd (if library is detected)
tc

# Generate typechecking report
mkci-tc-report
# Generates: tc_badge.svg, tc_report.html
```

## Tool: mkcodeclimate

The following tool is used to generate a [codeclimate](https://codeclimate.com/) report from linter output:
```bash
# Transforms output from linters to a codeclimate compatible document
lint --builder | mkcodeclimate -s $(pwd)
```
