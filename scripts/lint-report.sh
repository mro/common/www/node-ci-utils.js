#!/usr/bin/env bash

CWD=$(pwd)
[ -n "$TITLE" ] || TITLE='linting report'

echo '
<!doctype html>
<html>
  <head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body>'
echo '<div class="container">'
echo "<h1 class=\"display-4\">${CI_PROJECT_NAME} <small class=\"text-muted\">${TITLE}</small></h1>"
echo '<div class="list-group">'

while read LINT; do
  LIST_CLASS=""
  echo $LINT | grep -q 'warning' && LIST_CLASS="list-group-item-warning"
  LINT=$(echo $LINT | sed -e "s%${CWD}/%%")
  echo $LINT | sed -e 's%^\(\([/0-9a-zA-Z+._-]\{1,\}\):\([0-9]\{1,\}\):.*\)$%<a href="'${CI_PROJECT_URL}/blob/${CI_COMMIT_SHA}/'\2#L\3" class="list-group-item list-group-item-action '${LIST_CLASS}'">\1</a>%gp;d'
done


echo '</div></div></body></html>'
